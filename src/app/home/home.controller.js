'use strict';

angular.module('inspinia')
	.controller('HomeController', function () {
		
		var vm = this;
		
		vm.userName = 'Viktor Soltis';
		vm.helloText = 'Welcome in INSPINIA Gulp SeedProject';
		vm.descriptionText = 'It is an application skeleton for a typical AngularJS web app. You can use it to quickly bootstrap your angular webapp projects.';
		
	});
