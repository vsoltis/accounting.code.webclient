'use strict';

angular.module('inspinia')
	.controller('CurrencyController', function ($scope, $http, $q, $timeout) {

		/*
		currency rates http://fixer.io/, http://api.fixer.io/latest?base=USD
		all currencies https://docs.openexchangerates.org/docs/currencies-json
		 */

		$scope.currencies = [];
		$scope.newCurrency = {
			name: ''
		}
		$scope.selectedCurrency = {};

		var baseurl = 'http://localhost:6740';
		var url = "/api/currency";
		var delimeter = "/";

		$scope.gridOptions = {
			enableSorting: true,
			columnDefs: [
				{name: 'id', displayName: 'ID', width: '5%'},
				{name: 'name', displayName: 'Category Name', width: '70%'},
				{name: 'dateCreated', displayName: 'Created', width: '15%', cellFilter: 'date:\'MM-dd-yyyy\''},
				{name: 'Actions',
					cellTemplate: '<button class="btn btn-default btn-xs btn-danger btn-circle-micro btn-outline" ng-click="grid.appScope.deleteCategory(row)"><i class="fa fa-times"></i></button>',
					width: '10%'
				}
			]
		};

		$scope.deleteCurrency = function (row) {
			var id = row.entity.id;
			var CurrencyForDelete = {
				Id: row.entity.id,
				Name: row.entity.name
			};

			$http({
				method: 'DELETE',
				url: baseurl + url + delimeter + CurrencyForDelete.Id,
				data: $.param(CurrencyForDelete),
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).then(function (response) {
				console.log(response)
			});
			/*
			$http.delete(baseurl + url,  JSON.stringify(categoryForDelete))
				.then(function (response) {
					console.log(response);
				}, function (err) {
					// failure
					$scope.errorMessage = "Failed to load user";
				})
				.finally(function () {
						$scope.isBusy = false;
					}
				);*/

		}

		$scope.addCurrency = function () {
			var newCurrency = {
				Name: $scope.newCurrency.name,
				Code: $scope.newCurrency.code
			}

			$q.all([
					$http.post(baseurl + url, $.param(newCurrency), {
						headers: {
							'Content-Type': 'application/x-www-form-urlencoded'
						}
					})
					.then(function(response) {
						console.log(response);
					},
					function(response) { // optional
						// failed
					})
			]);

			$http.get(baseurl + url, {timeout: 1000})
				.then(function (response) {
					$scope.gridOptions.data = response.data;
					$scope.currencies = response.data;
					$scope.selectedCurrency = response.data[0];
				}, function (err) {
					// failure
					$scope.errorMessage = "Failed to load user";
				})
				.finally(function () {
					$scope.isBusy = false;
				}
			);

		}

		var allCurrenciesURL = "https://openexchangerates.org/api/currencies.json";

		$http.get(allCurrenciesURL)
			.success(function (response) {
				$scope.currencies = response;
				console.log(response);
			}, function (err) {
				// failure
				$scope.errorMessage = "Failed to load user";
			})
			.finally(function () {
				$scope.isBusy = false;
			})

	});
