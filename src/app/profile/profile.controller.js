'use strict';

angular.module('inspinia')
	.controller('ProfileController', function ($scope, $http) {

		var userId = "98cc5bf4-fc5e-426c-8e9d-8f1f39017db8";

		var baseurl = 'http://localhost:6740';
		var url = "/api/user/" + userId;

		$http.get(baseurl + url)
			.then(function (response) {
				// success
				$scope.user = response.data;
			}, function (err) {
				// failure
				$scope.errorMessage = "Failed to load user";
			})
			.finally(function () {
				$scope.isBusy = false;
			});
	});
