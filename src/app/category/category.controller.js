'use strict';

angular.module('inspinia')
	.controller('CategoryController', function ($scope, $http, $q, $timeout) {

		$scope.categories = [];
		$scope.newCategory = {
			name: '',
			isNested: false,
			categoryId: null
		}
		$scope.selectedCategory = {};

		var baseurl = 'http://localhost:6740';
		var url = "/api/category";
		var delimeter = "/";

		$scope.gridOptions = {
			enableSorting: true,
			columnDefs: [
				{name: 'id', displayName: 'ID', width: '5%'},
				{name: 'name', displayName: 'Category Name', width: '50%'},
				{name: 'parentCategory.name', displayName: 'Parent Category', width: '20%'},
				{name: 'dateCreated', displayName: 'Created', width: '15%', cellFilter: 'date:\'MM-dd-yyyy\''},
				{name: 'Actions',
					cellTemplate: '<button class="btn btn-default btn-xs btn-danger btn-circle-micro btn-outline" ng-click="grid.appScope.deleteCategory(row)"><i class="fa fa-times"></i></button>',
					width: '10%'
				}
			]
		};

		$scope.deleteCategory = function (row) {
			var id = row.entity.id;
			var categoryForDelete = {
				Id: row.entity.id,
				Name: row.entity.name,
				CategoryId: row.entity.categoryId
			};

			$http({
				method: 'DELETE',
				url: baseurl + url + delimeter + categoryForDelete.Id,
				data: $.param(categoryForDelete),
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).then(function (response) {
				console.log(response)
			});
			/*
			$http.delete(baseurl + url,  JSON.stringify(categoryForDelete))
				.then(function (response) {
					console.log(response);
				}, function (err) {
					// failure
					$scope.errorMessage = "Failed to load user";
				})
				.finally(function () {
						$scope.isBusy = false;
					}
				);*/

		}

		$scope.addCategory = function () {
			if ($scope.newCategory.isNested) {
				$scope.newCategory.categoryId = $scope.selectedCategory.id;
			} else {
				$scope.newCategory.categoryId = null;
			}

			var newCategory = {
				Name: $scope.newCategory.name,
				CategoryId: $scope.newCategory.categoryId
			}

			$q.all([
					$http.post(baseurl + url, $.param(newCategory), {
						headers: {
							'Content-Type': 'application/x-www-form-urlencoded'
						}
					})
					.then(function(response) {
						console.log(response);
					},
					function(response) { // optional
						// failed
					})
			]);

			$http.get(baseurl + url, {timeout: 1000})
				.then(function (response) {
					$scope.gridOptions.data = response.data;
					$scope.categories = response.data;
					$scope.selectedCategory = response.data[0];
				}, function (err) {
					// failure
					$scope.errorMessage = "Failed to load user";
				})
				.finally(function () {
					$scope.isBusy = false;
				}
			);

		}

		$q.all([
			$http.get(baseurl + url)
				.then(function (response) {
					$scope.gridOptions.data = response.data;
					$scope.categories = response.data;
					$scope.selectedCategory = response.data[0];
				}, function (err) {
					// failure
					$scope.errorMessage = "Failed to load user";
				})
				.finally(function () {
					$scope.isBusy = false;
				})
		]);

		/*
		 $scope.dtOptions = DTOptionsBuilder.newOptions()
		 .withDOM('<"html5buttons"B>lTfgitp')
		 .withButtons([
		 {extend: 'copy'},
		 {extend: 'csv'},
		 {extend: 'excel', title: 'ExampleFile'},
		 {extend: 'pdf', title: 'ExampleFile'},

		 {extend: 'print',
		 customize: function (win){
		 $(win.document.body).addClass('white-bg');
		 $(win.document.body).css('font-size', '10px');

		 $(win.document.body).find('table')
		 .addClass('compact')
		 .css('font-size', 'inherit');
		 }
		 }
		 ]);

		 */
	});
