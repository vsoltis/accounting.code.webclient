(function () {
	'use strict';

	angular
		.module('inspinia')
		.config(routerConfig);

	/** @ngInject */
	function routerConfig($stateProvider, $urlRouterProvider) {
		$stateProvider

			.state('index', {
				abstract: true,
				url: "/index",
				templateUrl: "app/components/common/content.html"
			})
			.state('index.home', {
				url: "/main",
				templateUrl: "app/home/home.html",
				data: {pageTitle: 'Dashboard'}
			})
			.state('index.minor', {
				url: "/minor",
				templateUrl: "app/minor/minor.html",
				data: {pageTitle: 'Example view'}
			})
			.state('user', {
				url: "/user",
				controllerAs: "UserController",
				templateUrl: "app/user/user.html",
				data: {pageTitle: 'User Page'},
				parent: "index"
			})
			.state('user.profile', {
				url: "/profile",
				controller: "ProfileController",
				templateUrl: "app/profile/profile.html",
				data: {pageTitle: 'User Profile Page'},
				parent: "index"
			})
			.state('account', {
				abstract: true,
				url: "/account",
				templateUrl: "app/components/common/content.html"
			})
			.state('account.categories', {
				url: "/account/categories",
				controller: "CategoryController",
				templateUrl: "app/category/category.html",
				data: {pageTitle: 'Categories'},
				parent: "account"
			})
			.state('account.currencies', {
				url: "/account/currencies",
				controller: "CurrencyController",
				templateUrl: "app/currency/currency.html",
				data: {pageTitle: 'Currencies'},
				parent: "account"
			});

		$urlRouterProvider.otherwise('/index/main');
	}

})();
